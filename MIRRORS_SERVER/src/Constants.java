
public class Constants
{
	static String portusedmessage = "port already in use";
	static String successmessage = "successful transfer to the client";
	static String invalidipmessage = "Entered IP address is not the primary IP address";
	static String invalidportmessage = "Entered available port number less than 65535";
	static String portinusemessage = "Enter a different port number. Port already in use";
	public static String ipaddressString = "ipaddress#";
	public static String portnumString = "port#";
}
