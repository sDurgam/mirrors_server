import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.nio.channels.ServerSocketChannel;
import java.util.Enumeration;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSplitPane;
import javax.swing.JTextArea;


public class ConnectIPPortFrame extends JFrame 
{
	JTextArea ipAddressText;
	JTextArea portNumberText;
	JLabel ipAddressLabel;
	JLabel portNumberLabel;
	public ConnectIPPortFrame()
	{
		JSplitPane ipAddressPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT);
		JSplitPane portNumberPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT);
		JSplitPane Panel01 = new JSplitPane(JSplitPane.VERTICAL_SPLIT);
		JSplitPane Panel02 = new JSplitPane(JSplitPane.VERTICAL_SPLIT);
		ipAddressLabel = new JLabel("Enter ip address:");
		ipAddressText =  new JTextArea(1,16);
		ipAddressPane.setTopComponent(ipAddressLabel);
		ipAddressPane.setBottomComponent(ipAddressText);
		portNumberLabel = new JLabel("Enter port number:");
		portNumberText = new JTextArea(1, 5);
		portNumberPane.setTopComponent(portNumberLabel);
		portNumberPane.setBottomComponent(portNumberText);
		JButton connectButton = new JButton("Connect to Server");

		connectButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent event)
			{
				//validate IP address and port number
				JButton connectBtn = (JButton)  event.getSource();
				connectBtn.setEnabled(false);
				String ipaddress = ipAddressText.getText();
				String portNumberStr = portNumberText.getText();
				int portNum = -1;
				String resultMsg = null;
				ServerSocketChannel server = null;
				if(ValidateIPAddress(ipaddress))
				{
					try
					{
						portNum = Integer.parseInt(portNumberStr);
					}
					catch(NumberFormatException nex)
					{
						JOptionPane.showMessageDialog(null, Constants.invalidportmessage, "Error", JOptionPane.ERROR_MESSAGE);
					}
					if(portNum != -1 && portNum <= 65535)
					{
						//check if the port number is available
						try
						{
							server = ServerSocketChannel.open();	
							server.configureBlocking(false);
							server.bind(new InetSocketAddress(ipaddress, portNum));
						}
						catch(IOException ex)
						{

							try
							{
								if(server != null)
								{
									server.close();
								}
							} catch (IOException e) 
							{
								e.printStackTrace();
							}
							connectBtn.setEnabled(true);
							JOptionPane.showMessageDialog(null, Constants.portusedmessage, "Error", JOptionPane.ERROR_MESSAGE);

						}
					}
					AudioRawBytesChannel bdata = new AudioRawBytesChannel();
					try 
					{
						resultMsg = bdata.WriteAudioRawBytesChannel(server);
					} 
					catch (IOException | LineUnavailableException e)
					{
						e.printStackTrace();
					}
					if(resultMsg != null && resultMsg.equals(Constants.portusedmessage))
					{
						connectBtn.setEnabled(true);
						JOptionPane.showMessageDialog(null, Constants.invalidportmessage, "Error", JOptionPane.ERROR_MESSAGE);
					}
				} 
				else
				{
					connectBtn.setEnabled(true);
					JOptionPane.showMessageDialog(null, Constants.invalidipmessage, "Error", JOptionPane.ERROR_MESSAGE);

				}
			}
		});
		Panel01.setTopComponent(ipAddressPane);
		Panel01.setBottomComponent(portNumberPane);
		Panel02.setTopComponent(Panel01);
		Panel02.setBottomComponent(connectButton);
		add(Panel02);
	}


	private boolean ValidateIPAddress(String userIPAddress)
	{
		boolean isValidIPAddress = false;
		Enumeration<NetworkInterface> netInterface = null;
		try
		{
			netInterface = NetworkInterface.getNetworkInterfaces();
		}
		catch(SocketException socketex)
		{
			return false;
		}
		NetworkInterface netelemet;
		Enumeration<InetAddress> ipAddressList;
		InetAddress ipAddress;
		while(netInterface.hasMoreElements())
		{
			netelemet = netInterface.nextElement();
			ipAddressList = netelemet.getInetAddresses();
			while(ipAddressList.hasMoreElements())
			{
				ipAddress = ipAddressList.nextElement();
				if(ipAddress instanceof Inet4Address)
				{
					if(ipAddress.isSiteLocalAddress() && ipAddress.getHostAddress().equals(userIPAddress))
					{
						isValidIPAddress = true;
						break;
					}
				}
			}
		}
		return isValidIPAddress;
	}
}
