import java.beans.Encoder;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.RandomAccessFile;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.nio.charset.Charset;
import java.nio.charset.CharsetEncoder;
import java.util.Iterator;
import java.util.Random;
import java.util.Set;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.DataLine;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.TargetDataLine;

public class AudioRawBytesChannel
{
	String WriteAudioRawBytesChannel(ServerSocketChannel server) throws IOException, LineUnavailableException
	{
		Selector selector = Selector.open();
		server.register(selector, SelectionKey.OP_ACCEPT);	
		SocketChannel client = null;	
		int numBytes;
		boolean isSocketConnected = true;
		byte[] dataBytes = new byte[1024];
		//	Region for virtual audio variables 	
		TargetDataLine targetLine;
		final AudioFormat format = new AudioFormat(AudioFormat.Encoding.PCM_SIGNED, 44100, 16, 2, 4, 44100, false);
		DataLine.Info info = new DataLine.Info(TargetDataLine.class, format);
		targetLine = (TargetDataLine) AudioSystem.getLine(info);
		targetLine.open();
		targetLine.start();
		//variables to write bytes on the client device
		File outputFile = new File("data_" + System.currentTimeMillis());
		outputFile.createNewFile();
		BufferedOutputStream bufferedWriter = new BufferedOutputStream(new FileOutputStream(outputFile));
		while(isSocketConnected)
		{
			selector.select();
			Set readyKeys = selector.selectedKeys();
			Iterator iterator = readyKeys.iterator();
			while (iterator.hasNext())
			{
				SelectionKey key = (SelectionKey) iterator.next();
				iterator.remove();
				if (key.isAcceptable())
				{
					client = server.accept();
					System.out.println("Accepted connection from " + client);
					client.configureBlocking(false);
					SelectionKey key2 = client.register(selector, SelectionKey.OP_WRITE);
					key2.attach(new Integer(100));
				} 
				else if (key.isWritable()) 
				{
					client = (SocketChannel) key.channel();
					//dataBytes = new byte[1024];
					numBytes = targetLine.read(dataBytes, 0, 1024);
					if(numBytes == -1)
					{ 
						isSocketConnected = false;
						key.cancel();
						break;
					}
					else
					{
						ByteBuffer output = ByteBuffer.wrap(dataBytes, 0, numBytes);
						try
						{
							client.write(output);
							bufferedWriter.write(dataBytes, 0, numBytes);
							if (!output.hasRemaining())
							{
								output.rewind();
							}
						}
						catch(Exception ex)
						{
							isSocketConnected = false;
							key.cancel();
							break;
						}
					}
				}
			}
		}
		targetLine.close();
		client.close();
		server.close();
		bufferedWriter.close();
		return Constants.successmessage;
	}


	public static byte[] toBytes(final int intVal) 
	{
		return ByteBuffer.allocate(4).putInt(intVal).array();
	}
}
