import java.io.IOException;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;
import javax.swing.JFrame;

public class Main 
{
	public static void main(String[] args) throws IOException, UnsupportedAudioFileException, LineUnavailableException
	{
		ConnectIPPortFrame ipAddressFrame = new ConnectIPPortFrame();
		ipAddressFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		ipAddressFrame.setSize(400, 200);
		ipAddressFrame.setVisible(true);
	}
}
